![Node.js CI](https://github.com/Braslerl/Discord-Multi-Bot/workflows/Node.js%20CI/badge.svg)
##  Selfhost Version

**The selfhost Version will be updated monthly, the public version I host gets more updates and have more features.**


##  Installation

  

[Download](https://github.com/Braslerl/Discord-Multi-Bot/archive/master.zip) or Clone the project.

  

To setup the Bot, create a Bot Token (Guide is [here](https://github.com/reactiflux/discord-irc/wiki/Creating-a-discord-bot-&-getting-a-token))

  

Put in you Token and Client ID in [this file](https://github.com/Braslerl/Discord-Multi-Bot/blob/master/data/settings.json), the default prefix is `!` you can change it by replacing here

![enter image description here](https://i.vgy.me/xdgY2a.png)

You need to install Node.js to run the bot, you can download it [here](https://nodejs.org/en/download/).

  

Go in the folder and click the `RUN.bat` file.

It should open a commandline and run `node bot.js`. When a error occurs it will be logged there.

When you found a fault, problems with setup or have questions, create a issue and I help you.

  

  

##  Update

To update, you only have to replace the content from you \data\commands.json with the updated content from the github repo, same with \data\events.json.

Nothing should be deleted, after this you can normally start the bot with the `RUN.bat` file.

  

<div id="blackRow">&nbsp;</div>

<head>
<script data-ad-client="ca-pub-8026097928580759" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
</head>
<!--stackedit_data:
eyJoaXN0b3J5IjpbMzI2MDE3MTMwLDk2OTMxMzY5NywtMTEwOT
YyNDgxNywxNTc1NzAzMDcyLC0xODI4ODQ2MDM5XX0=
-->