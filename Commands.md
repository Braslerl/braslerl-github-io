

#  Functions:

  

- **GIVEAWAY** (Because it was very buggy, I'll rewrite it next time. It's removed at the moment)
- **STATS**
- **MUTESYSTEM**
- **WARNSYSTEM**
- **FUN**
- **MOD**
- **TICKETTOOL**
- **WELCOME MESSAGE**
- **AUTOROLE**

**MUTE and WARN SYSTEM**

```

!setupmute to setup a Muted role, not needed if you have a Muted role, the name from this role must be exactly "Muted"!!!

!logs [#channel] to setup the log channel

!mute [@user] to mute the mentioned user

!unmute [@user] to unmute the mentioned user

!setwarns [@user] [amount] to give the mention user [amount] warns

!warn [@user] [reason] to warn the mentioned user

!warns [@user] to see the warning points

```

 

**When the user hits 3 warns, he will be banned**

  **MOD**

```
!botinfo to see the bot info
!lock lock the channel
!unlock unlock the channel
!whois [@user] see user infos
!ban [@user] ban the mentioned user
!kick [@user] kick the mentioned user
!channel-info [#channel] to see the channel info
!role-info   [role]             to see the role info
!saferoles enable/disable the save role function, when a user leave his roles get saved, when he join again he get the same roles as before
!set-welcome to set a welcome message and channel
!setstats to run the stat setup
```

![enter image description here](https://i.vgy.me/ULymqK.png)

```
!rolemenu Prompts you with a prompt to go through to add differnt roles and emoijs
```
  ![enter image description here](https://i.vgy.me/ShNENi.png)

**FUN**

```

!weather [location] see the weather at a location
Just try them ;-)
!wanted
!ship
!delete
!brazzers
!beautiful
!clap
!kill
!meme
!cuddle
!avatar
!hack
!qr
!cat
!password
!ascii
!rtx
!cmm
!calculator
!catfact
!wish
```

  

**TICKETTOOL**

```

!supportsetup to setup the ticket system, give the Support Role to the Support team with access to the tickets
```
*Do not change the name of the `new-ticket` channel*
```
!add [@user] [#ticket] adds the user to the ticket
!close run it in a ticket and it will be closed, can only be run by TICKET CREATOR
!forceclose can only be run by Support Team and will close the ticket in which it is run 
!inactive put the ticket inactive
!active put the ticket activ again
!new create a new ticket
!reactSetup setup a panel to create new tickets if someone reacts
!req get a log file
```

![enter image description here](https://i.vgy.me/RvoB31.png)

**AUTOROLE**
```
!set-autorole [@role] to setup a autorole, all member get the role when they join

!disable-autorole
```
![enter image description here](https://i.vgy.me/hhYVVs.png)

<head>
<script data-ad-client="ca-pub-8026097928580759" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
</head>
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTI2MDI2ODI3NSw5NDU1NjIyLDEzODg3OT
Y1MzYsLTE1MzY3NTQxNzEsLTE5NTAwNTA2NTAsLTY3NDY1NjI3
Nl19
-->