---
layout: post
title: Bot Status
subtitle: Embed for the status
cover-img: /assets/img/logo.png
tags: [new, page, status]
---

## Just a embed field with the status :)


<a href="https://top.gg/bot/708289725894754334" >
  <img src="https://top.gg/api/widget/708289725894754334.svg" alt="Multi Bot" />
</a>

<!--stackedit_data:
eyJoaXN0b3J5IjpbLTExMTQ4NTk2OTIsLTcwMDU4MzIyNiw3Nz
k2NDE5NTJdfQ==
-->