## How to start:

 -  `!logs [#channel]` to set the logging channel for ban, kick, mute, warn
 - `!set-welcome` to setup a welcome message, more infos when you run the command
 - `!set-welcome-image` to set the channel where a welcome image should be send
 - `!setupmutedrole` required for the mute command


 - **Ticketsystem:** (needs administrator permissions atm)
	 -  `!supportsetup`to setup the ticket system, give the Support Role to the Support team with access to the tickets
	 - `!reactSetup` setup a panel to create new tickets if someone reacts


 - **Autorole:**
	 - `!set-autorole [@role] [@role1] `to setup a autorole, all member get the role when they join
	 - `!disable-autorole` to disable it again

 - **Saferoles:**

	 -  ```!saferoles```  enable/disable the save role function, when a user leave his roles get saved, when he join again he get the same roles as before



<head>
<script data-ad-client="ca-pub-8026097928580759" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
</head>
<!--stackedit_data:
eyJoaXN0b3J5IjpbOTMwODE0NTU5LDE0NjYyMDY2ODAsNTM0OT
c0NDA5LDE4NzI0NjY5NjIsMTkzNDUyMzY3MiwtMjA4ODc0NjYx
Ml19
-->